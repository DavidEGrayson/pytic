# pytic

Proof of concept

Pure Python access to Pololu TIC stepper controllers

### Notes

Works on Windows or Linux

Uses PyUSB and libusb 

Easiest way to install drivers on Windows is to use Zadiag 


### References:

https://github.com/wholden/pololu-tic-python-ctypes
https://github.com/pololu/pololu-tic-software


